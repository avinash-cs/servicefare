const express = require('express');
const cookieParser = require('cookie-parser');
const fileUpload = require('express-fileupload');
const path = require('path');
const app = express();

// error Middleware
const errorMiddlerware = require('./middlewares/error');

app.use(express.json());
app.use(cookieParser());
app.use(fileUpload());

// setting up routes
const user = require('./routes/userRoute');
const service = require('./routes/serviceRoutes');
const cart = require('./routes/cartRoutes');
const order = require('./routes/orderRoutes');

app.use('/api/v1/user', user);
app.use('/api/v1/service', service);
app.use('/api/v1/cart', cart);
app.use('/api/v1/order', order);

app.use(errorMiddlerware);

if(process.env.NODE_ENV === "production") {
    app.use(express.static("client/build"));
    app.get("/*", function(req, res) {
        res.sendFile(path.join(__dirname, "../client/build/index.html"));
      });
}

// app.use(express.static(path.join(__dirname, '../client/build')))
// app.get('/*', (req, res) => {
//     res.sendFile(path.join(__dirname, '../client/build'))
// })

module.exports = app;